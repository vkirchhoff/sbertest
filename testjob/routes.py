from aiohttp import web
from .controllers import create_job, check_job_status


def setup_routes(app):
    app.add_routes(
        [
            web.get('/api/job', create_job),  # for testing in browser
            web.get('/api/job/{uuid}', check_job_status),
        ]
    )
