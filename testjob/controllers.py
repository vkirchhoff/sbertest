import asyncio
from aiohttp import web
from .client import get_uuid
from .models import get_job, RecordNotFound
from .tasks import test_task


async def create_job(request):
    uuid = await get_uuid()
    asyncio.ensure_future(test_task(request, uuid))
    return web.Response(text=uuid)


async def check_job_status(request):
    async with request.app['db'].acquire() as conn:
        try:
            job = await get_job(conn, request.match_info['uuid'])
            return web.Response(text=f'Job {job.uuid}  status is {job.status}')
        except RecordNotFound:
            return web.Response(text=f'ERROR: Job {request.match_info["uuid"]} was not found')
