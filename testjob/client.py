import aiohttp
from testjob.settings import config

SERVICE_URL = config['urls']['uuid_service_url']


async def get_uuid(service_url=SERVICE_URL):
    async with aiohttp.ClientSession() as session:
        async with session.get(service_url) as resp:
            received_uuid = await resp.text()
            assert resp.status // 100 == 2  # 2XX codes only
            return received_uuid.strip()
