import asyncio
from .models import insert_job, update_job_status

STATUS_STARTED = 'Started'
STATUS_PHASE_1 = 'Complete at phase 1'
STATUS_FINISHED = 'Finished'


async def test_task(request, uuid):
    async with request.app['db'].acquire() as conn:
        await insert_job(conn, uuid, STATUS_STARTED)
        await asyncio.sleep(20)
        await update_job_status(conn, uuid, STATUS_PHASE_1)
        await asyncio.sleep(20)
        await update_job_status(conn, uuid, STATUS_FINISHED)
