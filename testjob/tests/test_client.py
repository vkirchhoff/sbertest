import asyncio
import pytest
import re
from testjob.client import get_uuid


@pytest.fixture
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()


def test_uuid_format(event_loop):
    test_uuid = event_loop.run_until_complete(get_uuid())
    assert re.match(r'[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}', test_uuid)
