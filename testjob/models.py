from sqlalchemy import MetaData, Table, Column, String


meta = MetaData()

job = Table(
    'job', meta,

    Column('uuid', String(200), nullable=False),
    Column('status', String(36), nullable=False)
)


class RecordNotFound(Exception):
    """Requested record in database was not found"""


async def get_job(conn, job_id):
    result = await conn.execute(
        job.select()
        .where(job.c.uuid == job_id))
    job_result = await result.first()
    if not job_result:
        raise RecordNotFound(f'Job uuid: {str(job)} does not exists')
    return job_result


async def insert_job(conn, job_id, job_status):
    await conn.execute(
        job.insert(),
        {'uuid': job_id, 'status': job_status}
    )


async def update_job_status(conn, job_id, job_status):
    await conn.execute(
        job.update()
        .where(job.c.uuid == job_id)
        .values(status=job_status)
    )