from sqlalchemy import create_engine, MetaData
from testjob.models import job
from testjob.settings import config

DSN = "postgresql://{user}:@{host}:{port}/{database}"


def create_tables(db_engine):
    meta = MetaData()
    meta.create_all(bind=db_engine, tables=(job, ))


if __name__ == '__main__':
    engine = create_engine(DSN.format(**config['db']))
    create_tables(engine)
